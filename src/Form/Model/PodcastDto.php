<?php

namespace App\Form\Model;

use App\Entity\Podcast;

class PodcastDto {

    public $title;
    public $base64Image;
    public $categories;


    public function __construct()
    {
        $this->categories = [];
    }

    public static function createFromPodcast(Podcast $podcast): self
    {
        $dto = new self();
        $dto->title = $podcast->getTitle();
        $dto->image = $podcast->getImage();
        return $dto;
    }

}

