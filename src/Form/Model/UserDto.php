<?php

namespace App\Form\Model;
use App\Entity\User;

class UserDto {

    
    public $email;
    public $password;

    public static function createFromUser(User $user): self
    {
        $dto = new self();
        $dto->email = $user->getEmail();
        $dto->password = $user->getPassword();
        return $dto;
    }

}