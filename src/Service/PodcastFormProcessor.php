<?php

namespace App\Service;

use App\Entity\Podcast;
use App\Form\Model\CategoryDto;
use App\Form\Model\PodcastDto;
use App\Form\Type\PodcastFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PodcastFormProcessor

{

    private $podcastManager;
    private $categoryManager;
    private $fileUploader;
    private $formFactory;

    public function __construct(
        PodcastManager $podcastManager,
        CategoryManager $categoryManager,
        FileUploader $fileUploader,
        FormFactoryInterface $formFactory
    )
    {
        $this->podcastManager = $podcastManager;
        $this->categoryManager = $categoryManager;
        $this->fileUploader = $fileUploader;
        $this->formFactory = $formFactory;
    }

    public function __invoke(Podcast $podcast, Request $request): array
    {
        $podcastDto = PodcastDto::createFromPodcast($podcast);

        $originalCategories = new ArrayCollection();
        foreach ($podcast->getCategories() as $category) {
            $categoryDto = CategoryDto::createFromCategory($category);
            $podcastDto->categories[] = $categoryDto;
            $originalCategories->add($categoryDto);
        }

        $form = $this->formFactory->create(PodcastFormType::class, $podcastDto);
        $form->handleRequest($request);
        if(!$form->isSubmitted()){
            return [null, 'Form is not submitted'];
        }

        if ($form->isValid()){

            //Remove Categories//
            foreach ($originalCategories as $originalCategoryDto) {
                if(!in_array($originalCategoryDto, $podcastDto->categories)){
                $category = $this->categoryManager->find($originalCategoryDto->id);
                $podcast->removeCategory($category);

                }
            }

            //Add categories//

            foreach ($podcastDto->categories as $newCategoryDto) {
                if (!$originalCategories->contains($newCategoryDto)){
                    $category = $this->categoryManager->find($newCategoryDto->id ?? 0);
                    if(!$category) {
                        $category = $this->categoryManager->create();
                        $category->setName($newCategoryDto->name);
                        $this->categoryManager->persist($category);
                        
                    }
                    $podcast->addCategory($category);
                }
            }

            $podcast->setTitle($podcastDto->title);
            if($podcastDto->base64Image){
                $filename = $this->fileUploader->uploadBase64File($podcastDto->base64Image);
                $podcast->setImage($filename);
            }
            $this->podcastManager->save($podcast);
            $this->podcastManager->reload($podcast);
            return [$podcast, null];
        }
        return [null, $form];

     }
}









