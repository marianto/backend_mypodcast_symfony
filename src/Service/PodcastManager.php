<?php
namespace App\Service;

use App\Entity\Podcast;
use App\Repository\PodcastRepository;
use Doctrine\ORM\EntityManagerInterface;

class PodcastManager


{
    private $em;
    private $podcastRepository;
    public function __construct(EntityManagerInterface $em, PodcastRepository $podcastRepository)
    {
        $this->em = $em;
        $this->podcastRepository = $podcastRepository;
    }

    public function find(int $id): ?Podcast

    {
        return $this->podcastRepository->find($id);
    }

    public function getRepository(): PodcastRepository
    {
        return $this->podcastRepository;
    }

    public function create(): Podcast

    {
        $podcast = new Podcast();
        return $podcast;
    }

    public function save(Podcast $podcast): Podcast
    {
        $this->em->persist($podcast);
        $this->em->flush();
        return $podcast;
    }


    public function reload(Podcast $podcast): Podcast
    {
        $this->em->refresh($podcast);
        return $podcast;
    }

    public function delete(Podcast $podcast){
        $this->em->remove($podcast);
        $this->em->flush();
    }

}