<?php

namespace App\Controller;

use App\Entity\Podcast;
use App\Repository\PodcastRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


class controllerDefault extends AbstractController
{

     /**
     * @Route("/podcasts", name="podcasts_get")
     */

    public function list(Request $request, PodcastRepository $podcastRepository){
        $podcasts = $podcastRepository->findAll();
        $podcastsAsArray = [];
        foreach ($podcasts as $podcast) {
            $podcastsAsArray[] = [
                'id' => $podcast->getId(),
                'title' => $podcast->getTitle(),
                'image' => $podcast->getImage()
            ];
        };
        $response = new JsonResponse();
        $response->setData([
            'sucess'=> true,
            'data' => $podcastsAsArray,
        ]);
        return $response;

    }

     /**
     * @Route("/podcast/create", name="create_podcast")
     */

    public function createPodcast(Request $request, EntityManagerInterface $em){
        $podcast = new Podcast();
        $response = new JsonResponse;
        $title = $request->get('title', null);
        if (empty($title)){
            $response->setData([
                'sucess'=> false,
                'error' => 'title cannot be empty',
                'data' => null
            ]);
            return $response;
        }
        $podcast->setTitle($title);
        $em->persist($podcast);
        $em->flush();
        $response->setData([
            'sucess' => true,
            'data' => [
                [
                    'id' => $podcast->getId(),
                    'title' => $podcast->getTitle()
                ]
            ]
        ]);
        return $response;
       
        

    }



}