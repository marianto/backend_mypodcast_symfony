<?php

namespace App\Controller\Api;


use App\Form\Model\UserDto;
use App\Form\Type\UserFormType;
use App\Service\UserManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractFOSRestController
{

     /**
     * @Rest\Get(path="/login")
     * @Rest\View(serializerGroups={"user"}, serializerEnableMaxDepthChecks=true)
     */

    public function getAction(
        UserManager $userManager
    ) {
        return $userManager->getRepository()->findAll();
    }

      /**
     * @Rest\Post(path="/login")
     * @Rest\View(serializerGroups={"user"}, serializerEnableMaxDepthChecks=true)
     */
    public function postAction(
        Request $request,
        UserManager $userManager
    ) {
        $userDto = new UserDto();
        $form = $this->createForm(UserFormType::class, $userDto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userManager->create();
            $user->setEmail($userDto->email);
            $user->setPassword($userDto->password);
            $userManager->save($user);
            return $user;
        }
        return $form;
    }

    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

  

  



}
