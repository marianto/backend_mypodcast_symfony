<?php

namespace App\Controller\Api;



use App\Entity\Category;
use App\Entity\Podcast;
use App\Form\Model\CategoryDto;
use App\Form\Model\PodcastDto;
use App\Form\Type\PodcastFormType;
use App\Repository\CategoryRepository;
use App\Repository\PodcastRepository;
use App\Service\FileUploader;
use App\Service\PodcastFormProcessor;
use App\Service\PodcastManager;
use Doctrine\Common\Collections\ArrayCollection;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations\View as AnnotationsView;
use FOS\RestBundle\View\View;
use League\Flysystem\FilesystemInterface;
use Symfony\Component\HttpFoundation\Response;

class PodcastsController extends AbstractFOSRestController

{
    /**
     * @Rest\Get(path="/podcasts")
     * @Rest\View(serializerGroups={"podcast"}, serializerEnableMaxDepthChecks=true)
     */

     public function getAction(
        PodcastManager $podcastManager
     ){
         return $podcastManager->getRepository()->findAll();
     }

     /**
     * @Rest\Get(path="/podcasts/{id}", requirements={"id"="\d+"})
     * @Rest\View(serializerGroups={"podcast"}, serializerEnableMaxDepthChecks=true)
     */
    public function getSingleAction(
        int $id,
        PodcastManager $podcastManager
    ) {
        $podcast = $podcastManager->find($id);
        if (!$podcast) {
            return View::create('Podcast not found', Response::HTTP_BAD_REQUEST);
        }
        return $podcast;
    }

     /**
     * @Rest\Post(path="/podcasts")
     * @Rest\View(serializerGroups={"podcast"}, serializerEnableMaxDepthChecks=true)
     */

     public function postAction(

        PodcastManager $podcastManager,
        PodcastFormProcessor $podcastFormProcessor,
        Request $request

     ){
         $podcast = $podcastManager->create();
         [$podcast, $error] = ($podcastFormProcessor)($podcast, $request);
         $statusCode = $podcast ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST;
         $data = $podcast ?? $error;
         return View::create($data, $statusCode);

        
     }

      /**
     * @Rest\Post(path="/podcasts/{id}", requirements={"id"="\d+"})
     * @Rest\View(serializerGroups={"podcast"}, serializerEnableMaxDepthChecks=true)
     */

     public function editAction(

        int $id,
        PodcastFormProcessor $podcastFormProcessor,
        PodcastManager $podcastManager,
        Request $request

     ){

        $podcast = $podcastManager->find($id);
        if (!$podcast){
            return View::create('Podcast not found', Response::HTTP_BAD_REQUEST);
        }

        [$podcast, $error] = ($podcastFormProcessor)($podcast, $request);
        $statusCode = $podcast ? Response::HTTP_CREATED : Response::HTTP_BAD_REQUEST;
        $data = $podcast ?? $error;
        return View::create($data, $statusCode);
    }

    /**
     * @Rest\Delete(path="/podcasts/{id}", requirements={"id"="\d+"})
     * @Rest\View(serializerGroups={"podcast"}, serializerEnableMaxDepthChecks=true)
     */
    public function deleteAction(
        int $id,
        PodcastManager $podcastManager
    ) {
        $podcast = $podcastManager->find($id);
        if (!$podcast) {
            return View::create('Podcast not found', Response::HTTP_BAD_REQUEST);
        }
        $podcastManager->delete($podcast);
        return View::create(null, Response::HTTP_NO_CONTENT);
    }






}
